package au.edu.swin.sdmd.suncalculatorjava.calc;

import android.util.Log;

import com.opencsv.CSVReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.TimeZone;

public class CsvLoader {
    private InputStream inputStream;
    private ArrayList<GeoLocation> geoLocations;
    public CsvLoader(InputStream inputStream){
        this.inputStream = inputStream;
        readFromFile();
    }

    private void readFromFile(){
        geoLocations = new ArrayList<>();
        String[] nextLine;
        try{
            CSVReader reader = new CSVReader(new BufferedReader(new InputStreamReader(inputStream)));
            while ((nextLine = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                String name = nextLine[0];
                Double latitude = Double.parseDouble(nextLine[1]);
                Double longitude = Double.parseDouble(nextLine[2]);
                TimeZone tz = TimeZone.getTimeZone(nextLine[3]);
                GeoLocation geoLocation = new GeoLocation(name, latitude, longitude, tz);
                geoLocations.add(geoLocation);
                Log.d("CSV", geoLocation.toString());
            }
        } catch (IOException ex){
            Log.d("CSV", ex.getMessage());
        }
    }

    public ArrayList<GeoLocation> getGeoLocations(){
        return geoLocations;
    }
}
