package org.vhom.multiconverter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void navigateToDistance(View view){
        Log.d("main-menu", "Navigating to Distance Activity");
        startActivity(new Intent(this, Distance.class));
    }

    public void navigateToTemperature(View view){
        Log.d("main-menu","Navigating to Temperature Activity");
        startActivity(new Intent(this, Temperature.class));
    }
}
