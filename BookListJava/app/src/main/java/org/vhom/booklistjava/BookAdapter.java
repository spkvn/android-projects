package org.vhom.booklistjava;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    private ArrayList<Book> mBooks;
    public BookAdapter(ArrayList<Book> books){
        mBooks = books;
    }

    public static class BookViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTitle;
        public TextView tvAuthor;
        public ImageView imCover;

        public BookViewHolder(View book){
            super(book);
            tvTitle  = (TextView) book.findViewById(R.id.tvBookTitle);
            tvAuthor = (TextView) book.findViewById(R.id.tvBookAuthor);
            imCover  = (ImageView) book.findViewById(R.id.ivBookImage);
        }
    }

    @Override
    public BookAdapter.BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View bookView = inflater.inflate(R.layout.book_row, parent, false);

        return new BookViewHolder(bookView);
    }

    @Override
    public void onBindViewHolder(BookAdapter.BookViewHolder bookViewHolder, int position){
        Book book = mBooks.get(position);

        TextView tvTitle = bookViewHolder.tvTitle;
        tvTitle.setText(book.getTitle());

        TextView tvAuthor = bookViewHolder.tvAuthor;
        tvAuthor.setText(book.getAuthor());

        ImageView imageView = bookViewHolder.imCover;
        imageView.setImageResource(book.getImage());
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mBooks.size();
    }
}
