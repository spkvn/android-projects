package org.vhom.foodswithforms;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Random;

public class Food implements Parcelable {
    private String name;
    private Integer image;
    private String imageName;
    private String imageLocation;
    private String imageDate;
    private String keywords;
    private Boolean share;
    private String obtainedByAddress;
    private Integer rating;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        public Food createFromParcel(Parcel in){
            return new Food(in);
        }
        public Food[] newArray(int size){
            return new Food[size];
        }
    };

    public Food( String aName, Integer aImage, String aImageName,
                 String aListOfKeywords, Boolean aShare, String aLocation,
                 String aObtainingAddress, Integer aRating, String aDate){
        name = aName;
        image = aImage;
        imageName = aImageName;
        imageLocation = aLocation;
        keywords = aListOfKeywords;
        share = aShare;
        obtainedByAddress = aObtainingAddress;
        rating = aRating;
        imageDate = aDate;
    }

    public Food(){
        name = selectFoodName();
        image = selectImage();
        imageName = selectFoodName();
        keywords =  selectFoodName() + ", " + selectFoodName() + ", " + selectFoodName();
        Random r = new Random();
        share = r.nextBoolean();
        imageLocation = "http://google.com/images?q=chonk-boyes";
        obtainedByAddress = "alberto.balsam@email.com";
        rating = r.nextInt(5);
        imageDate = "2018-09-01";
    }

    public static ArrayList<Food> generateFoodList(Integer num){
        ArrayList<Food> foods = new ArrayList<>();
        for(Integer index = 0; index < num; index++){
            foods.add(new Food());
        }
        return foods;
    }

    private static final String[] NAMES = {"Coffee","Flour","Sugar","Rice", "Carrot","Fish", "Chicken", "Water","Broccoli","Onion"};
    private String selectFoodName(){
        return NAMES[new Random().nextInt(NAMES.length)];
    }

    private static final Integer[] IMAGES = {
            R.drawable.coffee, R.drawable.flour, R.drawable.sugar,
            R.drawable.carrot, R.drawable.fish, R.drawable.chicken,
            R.drawable.water, R.drawable.broccoli, R.drawable.onion
    };
    private Integer selectImage(){
        return IMAGES[new Random().nextInt(IMAGES.length)];
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public Boolean getShare() {
        return share;
    }

    public void setShare(Boolean share) {
        this.share = share;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObtainedByAddress() {
        return obtainedByAddress;
    }

    public void setObtainedByAddress(String obtainedByAddress) {
        this.obtainedByAddress = obtainedByAddress;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageDate() {
        return imageDate;
    }

    public void setImageDate(String imageDate) {
        this.imageDate = imageDate;
    }

    public Intent getIntent(Context aContext) {
        Intent intent = new Intent(aContext, FoodEditActivity.class);
        intent.putExtra("FOOD", this);
        return intent;
    }

    public Food(Parcel parcel){
        setName(parcel.readString());
        setImage(parcel.readInt());
        setImageName(parcel.readString());
        setImageLocation(parcel.readString());
        setImageDate(parcel.readString());
        setKeywords(parcel.readString());
        Integer share = parcel.readInt();
        if(share >= 1){
            setShare(true);
        } else {
            setShare(false);
        }
        setObtainedByAddress(parcel.readString());
        setRating(parcel.readInt());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getName());
        parcel.writeInt(getImage());
        parcel.writeString(getImageName());
        parcel.writeString(getImageLocation());
        parcel.writeString(getImageDate());
        parcel.writeString(getKeywords());
        if (share) {
            parcel.writeInt(1);
        } else {
            parcel.writeInt(0);
        }
        parcel.writeString(getObtainedByAddress());
        parcel.writeInt(getRating());
    }

    @Override
    public String toString() {
        return "Food{name='" + getName() + "'" +
            "image='" + getImage() + "'" +
            "imageName='" + getImageName() + "'" +
            "imageLocation='" + getImageLocation() + "'" +
            "imageDate='" + getImageDate() + "'" +
            "keywords='" + getKeywords() + "'" +
            "share='" + getShare() + "'" +
            "obtainedByAddress='" + getObtainedByAddress() + "'" +
            "rating='" + getRating() + "'" +
            "}";
    }
}
