package org.vhom.foodswithforms;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Food> foods;
    RecyclerView rvFoods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        foods = Food.generateFoodList(10);
        setUpRecycler();
    }

    private void setUpRecycler() {
        rvFoods = (RecyclerView) findViewById(R.id.rvFoods);
        FoodsAdapter foodsAdapter = new FoodsAdapter(foods);
        rvFoods.setAdapter(foodsAdapter);
        rvFoods.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Food food = data.getExtras().getParcelable("EDITED-FOOD");
        int collectionIndex = data.getExtras().getInt("COLLECTION-INDEX");
        foods.set(collectionIndex, food);
    }
}
