package org.vhom.distanceconverter

class Converter (var miles:Float = 0f, var feet:Float = 0f, var inches:Float = 0f, var meters:Boolean = false) {
    fun out():Float{
        val totalInches:Float = inches + (feet * 12) + (miles * 5280 * 12)
        var centimeters:Float = totalInches * 2.54f

        if(meters){
            return centimeters / 100
        } else {
            return centimeters
        }
    }
}