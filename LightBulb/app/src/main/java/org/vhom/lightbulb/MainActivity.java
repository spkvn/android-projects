package org.vhom.lightbulb;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private boolean on = false;

    private View.OnClickListener imageClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            flickSwitch();
            updateImage();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            on = savedInstanceState.getBoolean("on");
            updateImage();
        }

        initializeUI();
    }

    protected void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("on", on);
        super.onSaveInstanceState(bundle);
    }

    private void initializeUI(){
        ImageView im = findViewById(R.id.ivBulb);
        im.setOnClickListener(imageClick);
    }


    private void flickSwitch(){
        on = !on;
    }

    private void updateImage(){
        ImageView im = findViewById(R.id.ivBulb);
        if( on ){
            im.setImageDrawable(getResources().getDrawable(R.drawable.on));
        } else {
            im.setImageDrawable(getResources().getDrawable(R.drawable.off));
        }
    }
}
