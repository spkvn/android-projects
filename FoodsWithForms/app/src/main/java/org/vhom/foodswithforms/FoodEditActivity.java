package org.vhom.foodswithforms;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FoodEditActivity extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener  {
    private String mImageName;
    private String mImageLocation;
    private String mKeywords;
    private String mObtainedBy;
    private String mDate;
    private Boolean mShare;
    private Integer mRating;
    private Integer mCollectionIndex;
    private Food    mFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_edit);
        checkForIntent();
        setUiElements();
    }

    private void checkForIntent(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            mFood = extras.getParcelable("FOOD");
            mCollectionIndex = extras.getInt("COLLECTION-INDEX");
        }
    }

    private void setUiElements() {
        setEditTextUp(R.id.etImageName, mFood.getImageName());
        setEditTextUp(R.id.etImageLocation, mFood.getImageLocation());
        setEditTextUp(R.id.etKeywords, mFood.getKeywords());
        setEditTextUp(R.id.etObtainedBy, mFood.getObtainedByAddress());
        setTextViewUp(R.id.tvDate, mFood.getImageDate());
        setToggleButtonUp(R.id.tbShare, mFood.getShare());
        setSeekBarUp(R.id.sbRating, mFood.getRating());
    }

    private boolean checkValid(){
        // check image name
        mImageName = getStringFromEditText(R.id.etImageName);
        if(mImageName.length() <= 0){
            Toast.makeText(this, "Image name unset", Toast.LENGTH_SHORT).show();
            return false;
        }

        // chcek image location is url
        try{
            mImageLocation = getStringFromEditText(R.id.etImageLocation);
            URL url = new URL(mImageLocation);
        } catch (MalformedURLException ex){
            Toast.makeText(this, "Malformed URL in image location", Toast.LENGTH_SHORT).show();
            return false;
        }

        // check obtained is email
        mObtainedBy = getStringFromEditText(R.id.etObtainedBy);
        Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher mat = pattern.matcher(mObtainedBy);
        if(!mat.matches()){
            Toast.makeText(this, "Obtained by is not email", Toast.LENGTH_SHORT).show();
            return false;
        }

        // check seekbar within 0 and 5
        mRating = ((SeekBar)findViewById(R.id.sbRating)).getProgress();
        if(mRating > 5 || mRating < 0){
            Toast.makeText(this, "Rating outside 0-5 range", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Remainder (keywords, date, share)
        mDate = getStringFromTextView(R.id.tvDate);
        mShare = ((ToggleButton) findViewById(R.id.tbShare)).isChecked();
        mKeywords = getStringFromEditText(R.id.etKeywords);
        return true;
    }


    // onClick callbacks
    public void save(View view){
        if(checkValid()){
            Intent returnIntent = new Intent();
            mFood.setImageName(mImageName);
            mFood.setImageLocation(mImageLocation);
            mFood.setKeywords(mKeywords);
            mFood.setImageDate(mDate);
            mFood.setShare(mShare);
            mFood.setObtainedByAddress(mObtainedBy);
            mFood.setRating(mRating);
            returnIntent.putExtra("EDITED-FOOD", mFood);
            returnIntent.putExtra("COLLECTION-INDEX", mCollectionIndex);
            setResult(1,returnIntent);
            finish();
        }
    }

    public function on

    // Datepicker methods
    public static class DatePickerFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(
                getActivity(),
                (DatePickerDialog.OnDateSetListener) getActivity(),
                year,
                month,
                day
            );
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(),"datePicker");
    }

    public void onDateSet(DatePicker view, int year, int month, int day){
        Calendar cal = new GregorianCalendar(year, month, day);
        TextView tv = findViewById(R.id.tvDate);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        tv.setText(dateFormat.format(cal.getTime()));
    }

    // conveinice methods
    private void setEditTextUp(Integer id, String value){
        EditText element = findViewById(id);
        element.setText(value);
    }
    private void setTextViewUp(Integer id, String value){
        TextView el = findViewById(id);
        el.setText(value);
    }
    private void setToggleButtonUp(Integer id, Boolean bool){
        ToggleButton element = findViewById(id);
        element.setChecked(bool);
    }
    private void setSeekBarUp(Integer id, Integer val){
        SeekBar element = findViewById(id);
        element.setProgress(val);
    }
    private String getStringFromEditText(Integer id){
        EditText el = findViewById(id);
        return el.getText().toString();
    }
    private String getStringFromTextView(Integer id){
        TextView el = findViewById(id);
        return el.getText().toString();
    }
}
