package org.vhom.booklistjava;

public class Book {
    private String  author;
    private String  title;
    private Integer image;

    public Book(String aAuthor, String aTitle, Integer aImage){
        author  = aAuthor;
        title   = aTitle;
        image   = aImage;
    }

    public Integer getImage() {
        return image;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }
}
