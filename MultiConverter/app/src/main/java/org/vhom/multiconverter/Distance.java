package org.vhom.multiconverter;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class Distance extends Activity {

    private double feet;
    private double inches;
    private double miles;
    private double centimeters;
    private boolean convertToMeters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        convertToMeters = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        if(savedInstanceState != null){
            feet = savedInstanceState.getDouble("feet");
            miles = savedInstanceState.getDouble("miles");
            inches = savedInstanceState.getDouble("inches");
            centimeters = savedInstanceState.getDouble("centimeters");
            convertToMeters = savedInstanceState.getBoolean("convertToMeters");
            setOutput();
        }
    }

    public void toggleMeters(View view){
        convertToMeters = fetchBoolFromToggleButton((ToggleButton) findViewById(R.id.tbMeters));
    }

    public void convert(View view){
        feet = fetchDoubleFromEditText((EditText)findViewById(R.id.etFeet));
        inches = fetchDoubleFromEditText((EditText)findViewById(R.id.etInches));
        miles = fetchDoubleFromEditText((EditText)findViewById(R.id.etMiles));

        double totalInches = inches + (feet * 12) + (miles * 5280 * 12);
        centimeters = totalInches * 2.54;

        setOutput();
    }

    public void setOutput() {
        TextView output = findViewById(R.id.tvMetricOutput);
        if(convertToMeters){
            output.setText(String.format("%f m", centimeters / 100));
        } else {
            output.setText(String.format("%f cm", centimeters));
        }
    }

    private double fetchDoubleFromEditText(EditText et){
        try {
            return Double.parseDouble(et.getText().toString());
        } catch (Exception e){
            return 0.0;
        }
    }

    private boolean fetchBoolFromToggleButton(ToggleButton toggleButton){
        return toggleButton.isChecked();
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        state.putDouble("feet", feet);
        state.putDouble("miles", miles);
        state.putDouble("inches", inches);
        state.putDouble("centimeters", centimeters);
        state.putBoolean("convertToMeters", convertToMeters);
        super.onSaveInstanceState(state);
    }
}
