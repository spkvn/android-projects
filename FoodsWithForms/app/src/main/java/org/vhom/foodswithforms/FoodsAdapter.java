package org.vhom.foodswithforms;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class FoodsAdapter extends RecyclerView.Adapter<FoodsAdapter.FoodViewHolder>{
    ArrayList<Food> foods;
    Context mContext;
    public FoodsAdapter(ArrayList<Food> aSetOfFoods){
        foods = aSetOfFoods;
    }

    public static class FoodViewHolder extends RecyclerView.ViewHolder{
        public ImageView imFoodImage;
        public TextView tvFoodName;
        public TextView tvKeywords;
        public TextView tvObtainer;
        public ProgressBar pbRating;

        public FoodViewHolder(View food){
            super(food);
            imFoodImage = food.findViewById(R.id.imFoodImage);
            tvFoodName = food.findViewById(R.id.tvFoodName);
            tvKeywords = food.findViewById(R.id.tvKeywords);
            tvObtainer = food.findViewById(R.id.tvObtainer);
            pbRating  = food.findViewById(R.id.pbRating);
        }
    };

    @Override
    public FoodsAdapter.FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        Context context = parent.getContext();
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        View foodView = inflater.inflate(R.layout.food_row,parent,false);
        return new FoodViewHolder(foodView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder foodViewHolder, final int position) {
        Food food = foods.get(position);

        TextView tvFoodName = foodViewHolder.tvFoodName;
        tvFoodName.setText(food.getImageName());

        TextView tvKeywords = foodViewHolder.tvKeywords;
        tvKeywords.setText(food.getKeywords());

        TextView tvObtainer = foodViewHolder.tvObtainer;
        tvObtainer.setText(food.getObtainedByAddress());

        ProgressBar pbRating = foodViewHolder.pbRating;
        pbRating.setProgress(food.getRating());

        ImageView imFood = foodViewHolder.imFoodImage;
        imFood.setImageResource(food.getImage());

        foodViewHolder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                try{
                    Food food = foods.get(position);
                    Toast.makeText(v.getContext(), food.getName(), Toast.LENGTH_SHORT).show();
                    Intent formIntent = food.getIntent(mContext);
                    formIntent.putExtra("COLLECTION-INDEX", position);
                    ((Activity) mContext).startActivityForResult(formIntent, 1);
                } catch (Exception e){
                    Log.d("Food-Adapter", e.getMessage());
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return foods.size();
    }
}
