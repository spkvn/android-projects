package org.vhom.distanceconverter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var converter: Converter = Converter(0f,0f,0f, false)

    fun editTextToFloat(input: EditText):Float{
        var inputVal:Float = 0f
        try{
            inputVal = input.text.toString().toFloat()
        } catch (e: NumberFormatException) {
            Log.e("NumberFormat", "${e.toString()}")
        }
        return inputVal
    }

    fun calculate(){
        val inchesEdit :EditText = findViewById(R.id.etInches)
        converter.inches = editTextToFloat(inchesEdit)
        val feetEdit :EditText = findViewById(R.id.etFeet)
        converter.feet =  editTextToFloat(feetEdit)
        val milesEdit :EditText = findViewById(R.id.etMiles)
        converter.miles = editTextToFloat(milesEdit)

        val conversion = converter.out().toString()

        val valueOut: TextView = findViewById(R.id.tvValue)
        valueOut.text = conversion
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialiseUI()
    }

    fun initialiseUI(){
        val convertButton:Button = findViewById(R.id.btConvert)
        convertButton.setOnClickListener(object: View.OnClickListener{
            override fun onClick(view: View?) {
                calculate()
            }
        })

        val metersSwitch:Switch = findViewById(R.id.swMeters)
        val unitText:TextView = findViewById(R.id.tvUnit)
        metersSwitch.setOnClickListener(object: View.OnClickListener{
            override fun onClick(view: View?) {
                if(converter.meters){
                    converter.meters = false
                    unitText.text = getString(R.string.centimeters)
                } else {
                    converter.meters = true
                    unitText.text = getString(R.string.meters)
                }
                calculate()
            }
        })
    }
}
