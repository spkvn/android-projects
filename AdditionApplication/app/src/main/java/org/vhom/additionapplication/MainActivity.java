package org.vhom.additionapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiseUI();
    }

    private void initialiseUI(){
        Button equals = findViewById(R.id.btEquals);
        equals.setOnClickListener(equalsListener);
    }

    private View.OnClickListener equalsListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            TextView sumView = findViewById(R.id.tvSum);
            try {
                EditText inputA = findViewById(R.id.etNumberOne);
                EditText inputB = findViewById(R.id.etNumberTwo);
                float a = Float.parseFloat(inputA.getText().toString());
                float b = Float.parseFloat(inputB.getText().toString());
                float sum = a + b;
                sumView.setText(String.format("%.2f", sum));
            } catch (NumberFormatException e){
                sumView.setText("! ERR");
            }

        }
    };
}
