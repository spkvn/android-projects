package au.edu.swin.sdmd.suncalculatorjava;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import au.edu.swin.sdmd.suncalculatorjava.calc.AstronomicalCalendar;
import au.edu.swin.sdmd.suncalculatorjava.calc.CsvLoader;
import au.edu.swin.sdmd.suncalculatorjava.calc.GeoLocation;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    ArrayList<GeoLocation> geoLocations;
    static final int ADD_LOCATION_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.basic_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_plus :
                // start up other activity
                Intent addIntent = new Intent(this, LocationFormActivity.class);
                startActivityForResult(addIntent, ADD_LOCATION_REQUEST);
                return true;
            default:
                // we dont know what happened, let the superclass figure it out
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ADD_LOCATION_REQUEST){
            // regardless of success or fail, we're re-checking the files.
            setUpLocationsSpinner();
        }
    }

    private void setUpLocationsSpinner(){
        Spinner spinner = (Spinner) findViewById(R.id.spLocations);

        // load locations from csv file in raw
        CsvLoader csvLoader = new CsvLoader(getResources().openRawResource(R.raw.au_locations));
        ArrayList<GeoLocation> defaultLocations = csvLoader.getGeoLocations();

        geoLocations = new ArrayList<GeoLocation>();

        // load locations from internal storage
        try {
            File userLocations = new File(getApplicationContext().getFilesDir(), "userLocations.txt");
            if(userLocations.exists()){
                CsvLoader csvLoaderTwo = new CsvLoader(new FileInputStream(userLocations));
                ArrayList<GeoLocation> userLocationList = csvLoaderTwo.getGeoLocations();
                geoLocations.addAll(userLocationList);
            }
        } catch (FileNotFoundException ex){
            // No worries, no file.
        }


        geoLocations.addAll(defaultLocations);


        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Generate list of names;
        List<String> locationsForSpinner = new ArrayList<String>();
        locationsForSpinner.add("N/A");
        for(Integer i = 0; i < geoLocations.size(); i++) {
            locationsForSpinner.add(geoLocations.get(i).getLocationName());
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, locationsForSpinner);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        for(GeoLocation location : geoLocations){
            if(item.equals(location.getLocationName())) {
                updateTime(Calendar.getInstance(), location);
                TextView tvLocationName = findViewById(R.id.locationTV);
                tvLocationName.setText(location.getLocationName());
                break;
            }
        }
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void initializeUI() {
        setUpLocationsSpinner();

        Toolbar myToolbar = findViewById(R.id.exampleToolbar);
        setSupportActionBar(myToolbar);

        DatePicker dp = findViewById(R.id.datePicker);
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        dp.init(year,month,day,dateChangeHandler); // setup initial values and reg. handler
        updateTime(year, month, day);
    }

    private void updateTime(int year, int monthOfYear, int dayOfMonth) {
        TimeZone tz = TimeZone.getDefault();
        GeoLocation geolocation = new GeoLocation("Melbourne", -37.50, 145.01, tz);
        AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
        ac.getCalendar().set(year, monthOfYear, dayOfMonth);
        Date srise = ac.getSunrise();
        Date sset = ac.getSunset();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        TextView sunriseTV = findViewById(R.id.sunriseTimeTV);
        TextView sunsetTV = findViewById(R.id.sunsetTimeTV);
        Log.d("SUNRISE Unformatted", srise+"");

        sunriseTV.setText(sdf.format(srise));
        sunsetTV.setText(sdf.format(sset));
    }

    private void updateTime(Calendar cal, GeoLocation loc){
        TimeZone tz = loc.getTimeZone();
        AstronomicalCalendar ac = new AstronomicalCalendar(loc);
        ac.getCalendar().set(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        Date srise = ac.getSunrise();
        Date sset  = ac.getSunset();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        TextView sunriseTV = findViewById(R.id.sunriseTimeTV);
        TextView sunsetTV = findViewById(R.id.sunsetTimeTV);
        Log.d("SUNRISE Unformatted", srise+"");

        sunriseTV.setText(sdf.format(srise));
        sunsetTV.setText(sdf.format(sset));
    }

    DatePicker.OnDateChangedListener dateChangeHandler = new DatePicker.OnDateChangedListener()
    {
        public void onDateChanged(DatePicker dp, int year, int monthOfYear, int dayOfMonth)
        {
            updateTime(year, monthOfYear, dayOfMonth);
        }
    };


}
