package org.vhom.booklistjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private ArrayList<Book> books;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeBooks();
        setUpRecycler();
    }

    private void initializeBooks() {
        books = new ArrayList<>();
        books.add(new Book(getString(R.string.nc), getString(R.string.wrtw), R.drawable.nc_wrtw));
        books.add(new Book(getString(R.string.dfw), getString(R.string.ctl), R.drawable.dfw_ctl));
        books.add(new Book(getString(R.string.scip_authors), getString(R.string.scip), R.drawable.scip));
        books.add(new Book(getString(R.string.crcj), getString(R.string.sad), R.drawable.cr_cj_sad));
        books.add(new Book("An Author", "That's all the books i've read in a while", R.mipmap.ic_launcher));
        books.add(new Book("An Author", "whoops . . .", R.mipmap.ic_launcher_round));
        books.add(new Book("An Author", "That's all the books i've read in a while", R.mipmap.ic_launcher));
        books.add(new Book("An Author", "whoops . . .", R.mipmap.ic_launcher_round));
        books.add(new Book("An Author", "That's all the books i've read in a while", R.mipmap.ic_launcher));
        books.add(new Book("An Author", "whoops . . .", R.mipmap.ic_launcher_round));
        books.add(new Book("An Author", "That's all the books i've read in a while", R.mipmap.ic_launcher));
        books.add(new Book("An Author", "whoops . . .", R.mipmap.ic_launcher_round));

    }

    private void setUpRecycler() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rvBooks);

        BookAdapter bookAdapter = new BookAdapter(books);
        mRecyclerView.setAdapter(bookAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}