package au.edu.swin.sdmd.suncalculatorjava;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileWriter;
import java.io.IOException;
import java.util.TimeZone;

public class LocationFormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_form);
        Toolbar myToolbar = findViewById(R.id.exampleToolbar);
        setSupportActionBar(myToolbar);
    }

    public void saveInput(View view){
        String name = getStringFromEditText((EditText)findViewById(R.id.etName));
        Float lat = getFloatFromEditText((EditText)findViewById(R.id.etLat));
        Float lng = getFloatFromEditText((EditText)findViewById(R.id.etLong));
        String tz   = getStringFromEditText((EditText)findViewById(R.id.etTimezone));
        if(checkValid(name, lat, lng, tz)){
            writeToFile(name, lat, lng, tz);
        }
    }

    private boolean checkValid(String name, Float lat, Float lng, String tz){
        boolean valid = true;
        if(lat < -90 || lat > 90){
            Snackbar snackbar = Snackbar.make(
                    findViewById(R.id.locationLayout),
                    "Latitude must be in range -90° to 90°",
                    Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }

        if(lng < -180 || lng > 180){
            Snackbar snackbar = Snackbar.make(
                    findViewById(R.id.locationLayout),
                    "Longitude must be in range -180° to 180°",
                    Snackbar.LENGTH_LONG);
            snackbar.show();
            return false;
        }
        return valid;
    }

    private void writeToFile(String name, float lat, float lng, String tz){
        File file = new File(getApplicationContext().getFilesDir(), "userLocations.txt");
        try{
            CSVWriter csvWriter = new CSVWriter(new FileWriter(file, true));
            String[] locLine = { name, Float.toString(lat), Float.toString(lng), tz };
            csvWriter.writeNext(locLine);
            csvWriter.flush();
            csvWriter.close();
            Log.d("CSV-WRITE", "Wrote csv to " + file.getAbsolutePath());
        } catch (IOException ex){
            Toast.makeText(this, "Error Saving: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String getStringFromEditText(EditText editText) {
        return editText.getText().toString();
    }

    private Float getFloatFromEditText(EditText editText){
        return Float.parseFloat(editText.getText().toString());
    }

}
