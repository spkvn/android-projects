package org.vhom.threadstask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view){
        TextView status = findViewById(R.id.tvTime);
        try {
            for(int i = 3; i >= 0; i--){
                Thread.sleep(1000);
                status.setText(Integer.toString(i));
            }
        } catch (InterruptedException ie) {
            Log.d("INTERRUPT", ie.getMessage());
            ie.printStackTrace();
        }

    }
}
