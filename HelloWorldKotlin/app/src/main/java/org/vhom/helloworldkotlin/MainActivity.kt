package org.vhom.helloworldkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val a: Int = 325
        val b: Int = 231
        Log.d("MainActivity@kotlin", "Sum of $a and $b is ${sum(a,b)}")

        Log.d("MainActivity@java","Sum of $a and $b is ${Numbers.sum(a, b)}")
    }


    private fun sum(a: Int, b: Int): Int {
        return a + b
    }
}
