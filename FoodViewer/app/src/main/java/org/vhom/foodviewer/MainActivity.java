package org.vhom.foodviewer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    
    public void clickApple(View view){
        Intent appleShow = new Intent(this, FoodShow.class);
        Bundle bundle = new Bundle();
        bundle.putInt("Title", R.string.apple);
        bundle.putInt("Description", R.string.appleDesc);
        bundle.putInt("Image", R.drawable.apple);
        appleShow.putExtras(bundle);
        startActivity(appleShow);
    }

    public void clickMango(View view){
        Intent mangoShow = new Intent(this, FoodShow.class);
        Bundle bundle = new Bundle();
        bundle.putInt("Title", R.string.mango);
        bundle.putInt("Description", R.string.mangoDesc);
        bundle.putInt("Image", R.drawable.mango);
        mangoShow.putExtras(bundle);
        startActivity(mangoShow);
    }

    public void clickOrange(View view){
        Intent orangeShow = new Intent(this, FoodShow.class);
        Bundle bundle = new Bundle();
        bundle.putInt("Title", R.string.orange);
        bundle.putInt("Description",R.string.orangeDesc);
        bundle.putInt("Image", R.drawable.orange);
        orangeShow.putExtras(bundle);
        startActivity(orangeShow);
    }

    public void clickBroccoli(View view){
        Intent broccoliShow = new Intent(this, FoodShow.class);
        Bundle bundle = new Bundle();
        bundle.putInt("Title", R.string.broccoli);
        bundle.putInt("Description",R.string.broccoliDesc);
        bundle.putInt("Image", R.drawable.broccoli);
        broccoliShow.putExtras(bundle);
        startActivity(broccoliShow);
    }
}
