package org.vhom.foodviewer;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

public class FoodShow extends Activity {
    private String title;
    private String description;
    private Integer image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_show);
        checkForIntent();
        setViewValues();
    }

    private void setViewValues(){
        TextView tv = findViewById(R.id.tvTitle);
        tv.setText(title);
        TextView tvDesc = findViewById(R.id.tvDescription);
        tvDesc.setText(description);
        ImageView iv = findViewById(R.id.ivImage);
        iv.setImageResource(image);
    }

    private void checkForIntent(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            Integer iTitle = extras.getInt("Title");
            if(iTitle != null){
                title = getString(iTitle);
            }
            Integer iDesc = extras.getInt("Description");
            if(iDesc != null){
                description = getString(iDesc);
            }
            Integer iImage = extras.getInt("Image");
            if(iImage != null){
                image = iImage;
            }
        }
    }

}
