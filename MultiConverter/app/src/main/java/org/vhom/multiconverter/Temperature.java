package org.vhom.multiconverter;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class Temperature extends Activity {

    private double celsius = 0;
    private double fahrenheit = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        if(savedInstanceState != null){
            celsius = savedInstanceState.getDouble("celsius");
            fahrenheit = savedInstanceState.getDouble("fahrenheit");
            setOutput();
        }
    }

    private void setOutput(){
        TextView fahrenheitOutput = findViewById(R.id.tvFahrenheitOutput);
        try {
            String fahrenheitString = String.format(Locale.ENGLISH,"%.2f", fahrenheit) + "f°";
            fahrenheitOutput.setText(fahrenheitString);
        } catch(NumberFormatException e){
            fahrenheitOutput.setText("ERR");
        }
    }

    public void convert(View view){
        EditText celsiusInput = findViewById(R.id.etCecsius);
        celsius = Float.parseFloat(celsiusInput.getText().toString());
        fahrenheit = (celsius * (9.0/5.0) ) + 32.0;
        setOutput();
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        state.putDouble("celsius", celsius);
        state.putDouble("fahrenheit", fahrenheit);
        super.onSaveInstanceState(state);
    }
}
